﻿
namespace П_ятнашки_на_класах
{
    partial class GameForm6x6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameForm6x6));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuStart6x6 = new System.Windows.Forms.ToolStripMenuItem();
            this.Ruls6x6 = new System.Windows.Forms.ToolStripMenuItem();
            this.BackToMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.button35 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.TableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuStart6x6,
            this.Ruls6x6,
            this.BackToMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(785, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuStart6x6
            // 
            this.MenuStart6x6.Name = "MenuStart6x6";
            this.MenuStart6x6.Size = new System.Drawing.Size(103, 20);
            this.MenuStart6x6.Text = "Замішати поле";
            this.MenuStart6x6.Click += new System.EventHandler(this.MenuStart6x6_Click);
            // 
            // Ruls6x6
            // 
            this.Ruls6x6.Name = "Ruls6x6";
            this.Ruls6x6.Size = new System.Drawing.Size(67, 20);
            this.Ruls6x6.Text = "Правила";
            this.Ruls6x6.Click += new System.EventHandler(this.Ruls6x6_Click);
            // 
            // BackToMenu
            // 
            this.BackToMenu.Name = "BackToMenu";
            this.BackToMenu.Size = new System.Drawing.Size(142, 20);
            this.BackToMenu.Text = "Повернутись до меню";
            this.BackToMenu.Click += new System.EventHandler(this.BackToMenu_Click);
            // 
            // TableLayoutPanel
            // 
            this.TableLayoutPanel.ColumnCount = 6;
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.Controls.Add(this.button35, 5, 5);
            this.TableLayoutPanel.Controls.Add(this.button34, 4, 5);
            this.TableLayoutPanel.Controls.Add(this.button33, 3, 5);
            this.TableLayoutPanel.Controls.Add(this.button32, 2, 5);
            this.TableLayoutPanel.Controls.Add(this.button31, 1, 5);
            this.TableLayoutPanel.Controls.Add(this.button30, 0, 5);
            this.TableLayoutPanel.Controls.Add(this.button29, 5, 4);
            this.TableLayoutPanel.Controls.Add(this.button28, 4, 4);
            this.TableLayoutPanel.Controls.Add(this.button27, 3, 4);
            this.TableLayoutPanel.Controls.Add(this.button26, 2, 4);
            this.TableLayoutPanel.Controls.Add(this.button25, 1, 4);
            this.TableLayoutPanel.Controls.Add(this.button24, 0, 4);
            this.TableLayoutPanel.Controls.Add(this.button23, 5, 3);
            this.TableLayoutPanel.Controls.Add(this.button22, 4, 3);
            this.TableLayoutPanel.Controls.Add(this.button21, 3, 3);
            this.TableLayoutPanel.Controls.Add(this.button20, 2, 3);
            this.TableLayoutPanel.Controls.Add(this.button19, 1, 3);
            this.TableLayoutPanel.Controls.Add(this.button18, 0, 3);
            this.TableLayoutPanel.Controls.Add(this.button17, 5, 2);
            this.TableLayoutPanel.Controls.Add(this.button16, 4, 2);
            this.TableLayoutPanel.Controls.Add(this.button15, 3, 2);
            this.TableLayoutPanel.Controls.Add(this.button14, 2, 2);
            this.TableLayoutPanel.Controls.Add(this.button13, 1, 2);
            this.TableLayoutPanel.Controls.Add(this.button12, 0, 2);
            this.TableLayoutPanel.Controls.Add(this.button11, 5, 1);
            this.TableLayoutPanel.Controls.Add(this.button10, 4, 1);
            this.TableLayoutPanel.Controls.Add(this.button9, 3, 1);
            this.TableLayoutPanel.Controls.Add(this.button8, 2, 1);
            this.TableLayoutPanel.Controls.Add(this.button7, 1, 1);
            this.TableLayoutPanel.Controls.Add(this.button6, 0, 1);
            this.TableLayoutPanel.Controls.Add(this.button5, 5, 0);
            this.TableLayoutPanel.Controls.Add(this.button4, 4, 0);
            this.TableLayoutPanel.Controls.Add(this.button3, 3, 0);
            this.TableLayoutPanel.Controls.Add(this.button2, 2, 0);
            this.TableLayoutPanel.Controls.Add(this.button1, 1, 0);
            this.TableLayoutPanel.Controls.Add(this.button0, 0, 0);
            this.TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel.Location = new System.Drawing.Point(0, 24);
            this.TableLayoutPanel.Name = "TableLayoutPanel";
            this.TableLayoutPanel.RowCount = 6;
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.TableLayoutPanel.Size = new System.Drawing.Size(785, 572);
            this.TableLayoutPanel.TabIndex = 1;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button35.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button35.Location = new System.Drawing.Point(653, 478);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(129, 91);
            this.button35.TabIndex = 55;
            this.button35.Tag = "35";
            this.button35.UseVisualStyleBackColor = false;
            this.button35.Click += new System.EventHandler(this.button0_Click);
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button34.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button34.Location = new System.Drawing.Point(523, 478);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(124, 91);
            this.button34.TabIndex = 54;
            this.button34.Tag = "34";
            this.button34.UseVisualStyleBackColor = false;
            this.button34.Click += new System.EventHandler(this.button0_Click);
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button33.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button33.Location = new System.Drawing.Point(393, 478);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(124, 91);
            this.button33.TabIndex = 53;
            this.button33.Tag = "33";
            this.button33.UseVisualStyleBackColor = false;
            this.button33.Click += new System.EventHandler(this.button0_Click);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button32.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button32.Location = new System.Drawing.Point(263, 478);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(124, 91);
            this.button32.TabIndex = 52;
            this.button32.Tag = "32";
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.button0_Click);
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button31.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button31.Location = new System.Drawing.Point(133, 478);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(124, 91);
            this.button31.TabIndex = 51;
            this.button31.Tag = "31";
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button0_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button30.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button30.Location = new System.Drawing.Point(3, 478);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(124, 91);
            this.button30.TabIndex = 50;
            this.button30.Tag = "30";
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button0_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button29.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button29.Location = new System.Drawing.Point(653, 383);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(129, 89);
            this.button29.TabIndex = 49;
            this.button29.Tag = "29";
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button0_Click);
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button28.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button28.Location = new System.Drawing.Point(523, 383);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(124, 89);
            this.button28.TabIndex = 48;
            this.button28.Tag = "28";
            this.button28.UseVisualStyleBackColor = false;
            this.button28.Click += new System.EventHandler(this.button0_Click);
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button27.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button27.Location = new System.Drawing.Point(393, 383);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(124, 89);
            this.button27.TabIndex = 47;
            this.button27.Tag = "27";
            this.button27.UseVisualStyleBackColor = false;
            this.button27.Click += new System.EventHandler(this.button0_Click);
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button26.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button26.Location = new System.Drawing.Point(263, 383);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(124, 89);
            this.button26.TabIndex = 46;
            this.button26.Tag = "26";
            this.button26.UseVisualStyleBackColor = false;
            this.button26.Click += new System.EventHandler(this.button0_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button25.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button25.Location = new System.Drawing.Point(133, 383);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(124, 89);
            this.button25.TabIndex = 45;
            this.button25.Tag = "25";
            this.button25.UseVisualStyleBackColor = false;
            this.button25.Click += new System.EventHandler(this.button0_Click);
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button24.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button24.Location = new System.Drawing.Point(3, 383);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(124, 89);
            this.button24.TabIndex = 44;
            this.button24.Tag = "24";
            this.button24.UseVisualStyleBackColor = false;
            this.button24.Click += new System.EventHandler(this.button0_Click);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button23.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button23.Location = new System.Drawing.Point(653, 288);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(129, 89);
            this.button23.TabIndex = 43;
            this.button23.Tag = "23";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button0_Click);
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button22.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button22.Location = new System.Drawing.Point(523, 288);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(124, 89);
            this.button22.TabIndex = 42;
            this.button22.Tag = "22";
            this.button22.UseVisualStyleBackColor = false;
            this.button22.Click += new System.EventHandler(this.button0_Click);
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button21.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button21.Location = new System.Drawing.Point(393, 288);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(124, 89);
            this.button21.TabIndex = 41;
            this.button21.Tag = "21";
            this.button21.UseVisualStyleBackColor = false;
            this.button21.Click += new System.EventHandler(this.button0_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button20.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button20.Location = new System.Drawing.Point(263, 288);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(124, 89);
            this.button20.TabIndex = 40;
            this.button20.Tag = "20";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.button0_Click);
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button19.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button19.Location = new System.Drawing.Point(133, 288);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(124, 89);
            this.button19.TabIndex = 39;
            this.button19.Tag = "19";
            this.button19.UseVisualStyleBackColor = false;
            this.button19.Click += new System.EventHandler(this.button0_Click);
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button18.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button18.Location = new System.Drawing.Point(3, 288);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(124, 89);
            this.button18.TabIndex = 38;
            this.button18.Tag = "18";
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button0_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button17.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button17.Location = new System.Drawing.Point(653, 193);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(129, 89);
            this.button17.TabIndex = 37;
            this.button17.Tag = "17";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button0_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button16.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button16.Location = new System.Drawing.Point(523, 193);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(124, 89);
            this.button16.TabIndex = 36;
            this.button16.Tag = "16";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button0_Click);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button15.Location = new System.Drawing.Point(393, 193);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(124, 89);
            this.button15.TabIndex = 35;
            this.button15.Tag = "15";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button0_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button14.Location = new System.Drawing.Point(263, 193);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(124, 89);
            this.button14.TabIndex = 34;
            this.button14.Tag = "14";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button0_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button13.Location = new System.Drawing.Point(133, 193);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(124, 89);
            this.button13.TabIndex = 33;
            this.button13.Tag = "13";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button0_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button12.Location = new System.Drawing.Point(3, 193);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(124, 89);
            this.button12.TabIndex = 32;
            this.button12.Tag = "12";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button0_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button11.Location = new System.Drawing.Point(653, 98);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(129, 89);
            this.button11.TabIndex = 31;
            this.button11.Tag = "11";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button0_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button10.Location = new System.Drawing.Point(523, 98);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(124, 89);
            this.button10.TabIndex = 30;
            this.button10.Tag = "10";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button0_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button9.Location = new System.Drawing.Point(393, 98);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(124, 89);
            this.button9.TabIndex = 29;
            this.button9.Tag = "9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button0_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button8.Location = new System.Drawing.Point(263, 98);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(124, 89);
            this.button8.TabIndex = 28;
            this.button8.Tag = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button0_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button7.Location = new System.Drawing.Point(133, 98);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(124, 89);
            this.button7.TabIndex = 27;
            this.button7.Tag = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button0_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6.Location = new System.Drawing.Point(3, 98);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(124, 89);
            this.button6.TabIndex = 26;
            this.button6.Tag = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button0_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.Location = new System.Drawing.Point(653, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(129, 89);
            this.button5.TabIndex = 25;
            this.button5.Tag = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button0_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Location = new System.Drawing.Point(523, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(124, 89);
            this.button4.TabIndex = 24;
            this.button4.Tag = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button0_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3.Location = new System.Drawing.Point(393, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 89);
            this.button3.TabIndex = 23;
            this.button3.Tag = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button0_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(263, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 89);
            this.button2.TabIndex = 22;
            this.button2.Tag = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button0_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(133, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 89);
            this.button1.TabIndex = 21;
            this.button1.Tag = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button0_Click);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button0.Location = new System.Drawing.Point(3, 3);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(124, 89);
            this.button0.TabIndex = 18;
            this.button0.Tag = "0";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // GameForm6x6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 596);
            this.Controls.Add(this.TableLayoutPanel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GameForm6x6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "П\'ятнашки6x6";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameForm6x6_FormClosing);
            this.Load += new System.EventHandler(this.GameForm6x6_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuStart6x6;
        private System.Windows.Forms.ToolStripMenuItem Ruls6x6;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem BackToMenu;
        private System.Windows.Forms.Button button35;
    }
}