﻿
namespace П_ятнашки_на_класах
{
    partial class MenuGameMode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuGameMode));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button3x3 = new System.Windows.Forms.Button();
            this.button4x4 = new System.Windows.Forms.Button();
            this.button5x5 = new System.Windows.Forms.Button();
            this.button6x6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.Location = new System.Drawing.Point(228, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(98, 101);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // button3x3
            // 
            this.button3x3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button3x3.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button3x3.Location = new System.Drawing.Point(157, 179);
            this.button3x3.Name = "button3x3";
            this.button3x3.Size = new System.Drawing.Size(247, 65);
            this.button3x3.TabIndex = 23;
            this.button3x3.Tag = "0";
            this.button3x3.Text = "Режим 3х3";
            this.button3x3.UseVisualStyleBackColor = false;
            this.button3x3.Click += new System.EventHandler(this.button3x3_Click);
            // 
            // button4x4
            // 
            this.button4x4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button4x4.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4x4.Location = new System.Drawing.Point(157, 250);
            this.button4x4.Name = "button4x4";
            this.button4x4.Size = new System.Drawing.Size(247, 65);
            this.button4x4.TabIndex = 24;
            this.button4x4.Tag = "0";
            this.button4x4.Text = "Режим 4х4";
            this.button4x4.UseVisualStyleBackColor = false;
            this.button4x4.Click += new System.EventHandler(this.button4x4_Click);
            // 
            // button5x5
            // 
            this.button5x5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button5x5.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5x5.Location = new System.Drawing.Point(157, 321);
            this.button5x5.Name = "button5x5";
            this.button5x5.Size = new System.Drawing.Size(247, 65);
            this.button5x5.TabIndex = 25;
            this.button5x5.Tag = "0";
            this.button5x5.Text = "Режим 5х5";
            this.button5x5.UseVisualStyleBackColor = false;
            this.button5x5.Click += new System.EventHandler(this.button5x5_Click);
            // 
            // button6x6
            // 
            this.button6x6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button6x6.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button6x6.Location = new System.Drawing.Point(157, 392);
            this.button6x6.Name = "button6x6";
            this.button6x6.Size = new System.Drawing.Size(247, 65);
            this.button6x6.TabIndex = 26;
            this.button6x6.Tag = "0";
            this.button6x6.Text = "Режим 6х6";
            this.button6x6.UseVisualStyleBackColor = false;
            this.button6x6.Click += new System.EventHandler(this.button6x6_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button4.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Location = new System.Drawing.Point(12, 27);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(127, 65);
            this.button4.TabIndex = 27;
            this.button4.Tag = "0";
            this.button4.Text = "Назад";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(120, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 37);
            this.label1.TabIndex = 28;
            this.label1.Text = "Оберіть режим гри";
            // 
            // MenuGameMode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(552, 484);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button6x6);
            this.Controls.Add(this.button5x5);
            this.Controls.Add(this.button4x4);
            this.Controls.Add(this.button3x3);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuGameMode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "П\'ятнашки Меню";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button3x3;
        private System.Windows.Forms.Button button4x4;
        private System.Windows.Forms.Button button5x5;
        private System.Windows.Forms.Button button6x6;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
    }
}