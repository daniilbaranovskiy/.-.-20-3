﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace П_ятнашки_на_класах
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Show();
            this.Hide();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Правила гри: гравець має розмістити цифри на платформі у порядку зростання кількість цифр буде залежати від вибраного режиму.");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
