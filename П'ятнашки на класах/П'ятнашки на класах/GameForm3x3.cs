﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameClassLibrary;


namespace П_ятнашки_на_класах
{
    public partial class GameForm3x3 : Form
    {
        GameClassShift game3x3;
        public GameForm3x3()
        {
            InitializeComponent();
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            game3x3 = new GameClassShift(3);
        }

        private void button0_Click(object sender, EventArgs e)
        {
            int pos = Convert.ToInt16(((Button)sender).Tag);
            game3x3.Shift(pos);
            Refresh3x3();
            if (game3x3.Win())
            {
                MessageBox.Show("Ви перемогли! Вітаю!");
            }
        }
        private Button button3x3(int pos)
        {
            switch (pos)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                default: return null;
            }
        }
        private void GameStart3x3()
        {
            game3x3.Start();
            for (int i = 0; i < 150; i++)
            {
                game3x3.ShiftRandom();
            }
            Refresh3x3();
        }
        private void GameLoad3x3()
        {
            game3x3.Start();
            Refresh3x3();
        }
        private void Refresh3x3()
        {
            for (int pos = 0; pos < 9; pos++)
            {
                int GN = game3x3.GetNumber(pos);
                button3x3(pos).Text = GN.ToString();
                button3x3(pos).Visible = (GN > 0);
            }
        }
        private void MenuStart3x3_Click(object sender, EventArgs e)
        {
            GameStart3x3();
        }

        private void Ruls3x3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Правила гри: гравець має розмістити цифри на платформі у порядку зростання від 1 до 8.");
        }

        private void GameForm3x3_Load(object sender, EventArgs e)
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            GameLoad3x3();
            TableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GameForm3x3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Menu newForm = new Menu();
            newForm.Show();
        }
    }
}
