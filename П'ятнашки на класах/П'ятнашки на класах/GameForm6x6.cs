﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameClassLibrary;


namespace П_ятнашки_на_класах
{
    public partial class GameForm6x6 : Form
    {
        GameClassShift game6x6;
        public GameForm6x6()
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            InitializeComponent();
            game6x6 = new GameClassShift(6);
        }
        private Button button6x6(int pos)
        {
            switch (pos)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;
                case 16: return button16;
                case 17: return button17;
                case 18: return button18;
                case 19: return button19;
                case 20: return button20;
                case 21: return button21;
                case 22: return button22;
                case 23: return button23;
                case 24: return button24;
                case 25: return button25;
                case 26: return button26;
                case 27: return button27;
                case 28: return button28;
                case 29: return button29;
                case 30: return button30;
                case 31: return button31;
                case 32: return button32;
                case 33: return button33;
                case 34: return button34;
                case 35: return button35;
                default: return null;
            }
        }
        private void GameStart6x6()
        {
            game6x6.Start();
            for (int i = 0; i < 250; i++)
            {
                game6x6.ShiftRandom();
            }
            Refresh6x6();
        }
        private void GameLoad6x6()
        {
            game6x6.Start();
            Refresh6x6();
        }
        private void Refresh6x6()
        {
            for (int pos = 0; pos < 36; pos++)
            {
                int GN = game6x6.GetNumber(pos);
                button6x6(pos).Text = GN.ToString();
                button6x6(pos).Visible = (GN > 0);
            }
        }
        private void GameForm6x6_Load(object sender, EventArgs e)
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            GameLoad6x6();
            TableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
        }

        private void MenuStart6x6_Click(object sender, EventArgs e)
        {
            GameStart6x6();
        }

        private void Ruls6x6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Правила гри: гравець має розмістити цифри на платформі у порядку зростання від 1 до 35.");
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GameForm6x6_FormClosing(object sender, FormClosingEventArgs e)
        {
            Menu newForm = new Menu();
            newForm.Show();
        }

        private void button0_Click(object sender, EventArgs e)
        {
            int pos = Convert.ToInt16(((Button)sender).Tag);
            game6x6.Shift(pos);
            Refresh6x6();
            if (game6x6.Win())
            {
                MessageBox.Show("Ви перемогли! Вітаю!");
            }
        }
    }
}
