﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameClassLibrary;

namespace П_ятнашки_на_класах
{
    public partial class GameForm4x4 : Form
    {
        GameClassShift game4x4;

        public GameForm4x4()
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            InitializeComponent();
            game4x4 = new GameClassShift(4);
        }
        private void button0_Click(object sender, EventArgs e)
        {
            int pos = Convert.ToInt16(((Button)sender).Tag);
            game4x4.Shift(pos);
            Refresh4x4();
            if(game4x4.Win())
            {
                MessageBox.Show("Ви перемогли! Вітаю!");
            }
        }
        private Button button4x4(int pos)
        {
            switch (pos)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;
                default: return null;
            }
        }
        private void GameStart4x4()
        {
            game4x4.Start();
            for (int i = 0; i < 150; i++)
            {
                game4x4.ShiftRandom();
            }
            Refresh4x4();
        }
        private void GameLoad4x4()
        {
            game4x4.Start();
            Refresh4x4();
        }
        private void Refresh4x4()
        {
            for (int pos = 0; pos < 16; pos++)
            {
                int GN = game4x4.GetNumber(pos);
                button4x4(pos).Text = GN.ToString();
                button4x4(pos).Visible = (GN > 0);
            }
        }
        private void GameForm_Load(object sender, EventArgs e)
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            GameLoad4x4();
            TableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
        }

        private void MenuStart4x4_Click(object sender, EventArgs e)
        {
            GameStart4x4();
        }

        private void Ruls4x4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Правила гри: гравець має розмістити цифри на платформі у порядку зростання від 1 до 15.");
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GameForm4x4_FormClosing(object sender, FormClosingEventArgs e)
        {
            Menu newForm = new Menu();
            newForm.Show();
        }
    }
}
