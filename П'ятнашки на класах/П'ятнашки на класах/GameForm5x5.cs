﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameClassLibrary;


namespace П_ятнашки_на_класах
{
    public partial class GameForm5x5 : Form
    {
        GameClassShift game5x5;
        public GameForm5x5()
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            InitializeComponent();
            game5x5 = new GameClassShift(5);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int pos = Convert.ToInt16(((Button)sender).Tag);
            game5x5.Shift(pos);
            Refresh5x5();
            if (game5x5.Win())
            {
                MessageBox.Show("Ви перемогли! Вітаю!");
            }
        }
        private Button button5x5(int pos)
        {
            switch (pos)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;
                case 16: return button16;
                case 17: return button17;
                case 18: return button18;
                case 19: return button19;
                case 20: return button20;
                case 21: return button21;
                case 22: return button22;
                case 23: return button23;
                case 24: return button24;
                default: return null;
            }
        }
        private void GameStart5x5()
        {
            game5x5.Start();
            for (int i = 0; i < 250; i++)
            {
                game5x5.ShiftRandom();
            }
            Refresh5x5();
        }
        private void GameLoad5x5()
        {
            game5x5.Start();
            Refresh5x5();
        }
        private void Refresh5x5()
        {
            for (int pos = 0; pos < 25; pos++)
            {
                int GN = game5x5.GetNumber(pos);
                button5x5(pos).Text = GN.ToString();
                button5x5(pos).Visible = (GN > 0);
            }
        }

        private void GameForm5x5_Load(object sender, EventArgs e)
        {
            MenuGameMode newForm = new MenuGameMode();
            newForm.Close();
            GameLoad5x5();
            TableLayoutPanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
        }

        private void MenuStart5x5_Click(object sender, EventArgs e)
        {
            GameStart5x5();
        }

        private void Ruls5x5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Правила гри: гравець має розмістити цифри на платформі у порядку зростання від 1 до 24.");
        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GameForm5x5_FormClosing(object sender, FormClosingEventArgs e)
        {
            Menu newForm = new Menu();
            newForm.Show();
        }
    }
}
