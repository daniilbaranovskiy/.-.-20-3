﻿using System;
using System.Windows.Forms;

namespace П_ятнашки_на_класах
{
    public partial class MenuGameMode : Form
    {
        public MenuGameMode()
        {

            InitializeComponent();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Menu newForm = new Menu();
            newForm.Show();
            this.Close();
        }

        private void button3x3_Click(object sender, EventArgs e)
        {
            GameForm3x3 newForm = new GameForm3x3();
            newForm.Show();
            this.Close();
        }

        private void button4x4_Click(object sender, EventArgs e)
        {
            GameForm4x4 newForm = new GameForm4x4();
            newForm.Show();
            this.Close();
        }

        private void button5x5_Click(object sender, EventArgs e)
        {
            GameForm5x5 newForm = new GameForm5x5();
            newForm.Show();
            this.Close();
        }

        private void button6x6_Click(object sender, EventArgs e)
        {
            GameForm6x6 newForm = new GameForm6x6();
            newForm.Show();
            this.Close();
        }
    }
}
