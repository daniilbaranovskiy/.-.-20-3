﻿using System;
namespace GameClassLibrary
{
    public class GameClassNumber : GameClassMap
    {
        protected int SpaceX;
        protected int SpaceY;
        public GameClassNumber(int Size) : base(Size)
        {
        }
        public void Start()
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    map[x, y] = CordsToPos(x, y) + 1;
                    SpaceX = Size - 1;
                    SpaceY = Size - 1;
                    map[SpaceX, SpaceY] = 0;
                }
            }
        }
        public int GetNumber(int pos)
        {
            int x, y;
            PosToCords(pos, out x, out y);
            if (x < 0 || x >= Size) return 0;
            if (y < 0 || y >= Size) return 0;
            return map[x, y];
        }
        public bool Win()
        {
            if (!(SpaceX == Size - 1 && SpaceY == Size - 1))
                return false;
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    if (!(x == Size - 1 && y == Size - 1))
                    {
                        if (map[x, y] != CordsToPos(x, y) + 1)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
