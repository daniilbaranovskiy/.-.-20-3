﻿using System;
namespace GameClassLibrary
{
    public class GameClassMap
    {
        public int Size;
        public int[,] map;
        public GameClassMap(int size)
        {
            this.Size = size;
            map = new int[size, size];
        }
        public int CordsToPos(int x, int y)
        {
            if (x < 0) x = 0;
            if (x > Size - 1) x = Size - 1;
            if (y < 0) y = 0;
            if (y > Size - 1) y = Size - 1;
            return y * Size + x;
        }
        public void PosToCords(int pos, out int x, out int y)
        {
            if (pos < 0) pos = 0;
            if (pos > Size * Size - 1) pos = Size * Size - 1;
            x = pos % Size;
            y = pos / Size;
        }
    }
}
