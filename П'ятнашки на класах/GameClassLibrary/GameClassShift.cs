﻿using System;
namespace GameClassLibrary
{
    public class GameClassShift : GameClassNumber
    {
        static Random rand = new Random();
        public GameClassShift(int Size) : base(Size)
        {
        }
        public void Shift(int pos)
        {
            int x, y;
            PosToCords(pos, out x, out y);
            if (Math.Abs(SpaceX - x) + Math.Abs(SpaceY - y) != 1)
            {
                return;
            }
            map[SpaceX, SpaceY] = map[x, y];
            map[x, y] = 0;
            SpaceX = x;
            SpaceY = y;
        }
        public void ShiftRandom()
        {
            int a = rand.Next(0, 4);
            int x = SpaceX;
            int y = SpaceY;
            switch (a)
            {
                case 0: x--; break;
                case 1: x++; break;
                case 2: y--; break;
                case 3: y++; break;
            }
            Shift(CordsToPos(x, y));
        }
    }
}
